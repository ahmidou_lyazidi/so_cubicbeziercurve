// Copyright (c) 2011 Mohammad Abdulfatah
// 
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
// 
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
// 
//     1. The origin of this software must not be misrepresented;
//        you must not claim that you wrote the original software.
//        If you use this software in a product, an acknowledgment
//        in the product documentation would be appreciated but is
//        not required.
// 
//     2. Altered source versions must be plainly marked as such,
//        and must not be misrepresented as being the original
//        software.
// 
//     3. This notice may not be removed or altered from any
//        source distribution.
//

#include <xsi_application.h>
#include <xsi_context.h>
#include <xsi_pluginregistrar.h>
#include <xsi_status.h>

#include <xsi_icenodecontext.h>
#include <xsi_icenodedef.h>
#include <xsi_command.h>
#include <xsi_factory.h>
#include <xsi_longarray.h>
#include <xsi_doublearray.h>
#include <xsi_math.h>
#include <xsi_vector2f.h>
#include <xsi_vector3f.h>
#include <xsi_vector4f.h>
#include <xsi_matrix3f.h>
#include <xsi_matrix4f.h>
#include <xsi_rotationf.h>
#include <xsi_quaternionf.h>
#include <xsi_color4f.h>
#include <xsi_shape.h>
#include <xsi_icegeometry.h>
#include <xsi_iceportstate.h>
#include <xsi_indexset.h>
#include <xsi_dataarray.h>
#include <xsi_dataarray2D.h>

#include <vector>

void ComputePositionOnBezeirCurve
	( float s
	, const XSI::MATH::CVector3f& p0
	, const XSI::MATH::CVector3f& p1
	, const XSI::MATH::CVector3f& p2
	, const XSI::MATH::CVector3f& p3
	, XSI::MATH::CVector3f* p
	);

void ComputeLinearSegmentsLengths
	( int segmentCount
	, const XSI::MATH::CVector3f& p0
	, const XSI::MATH::CVector3f& p1
	, const XSI::MATH::CVector3f& p2
	, const XSI::MATH::CVector3f& p3
	, std::vector<float>* lengths
	);

void ComputeTangentOnBezeirCurve
	( float s
	, const XSI::MATH::CVector3f& p0
	, const XSI::MATH::CVector3f& p1
	, const XSI::MATH::CVector3f& p2
	, const XSI::MATH::CVector3f& p3
	, XSI::MATH::CVector3f* t
	);

// Defines port, group and map identifiers used for registering the ICENode
enum IDs
{
	ID_IN_S = 0,
	ID_IN_P0 = 1,
	ID_IN_P1 = 2,
	ID_IN_P2 = 3,
	ID_IN_P3 = 4,
	ID_IN_SegmentCount = 5,
	ID_G_100 = 100,
	ID_OUT_Position = 200,
	ID_OUT_Tangent = 201,
	ID_OUT_Length = 202,
	ID_TYPE_CNS = 400,
	ID_STRUCT_CNS,
	ID_CTXT_CNS,
	ID_UNDEF = ULONG_MAX
};

XSI::CStatus RegisterSO_CubicBezierCurve( XSI::PluginRegistrar& in_reg );

using namespace XSI; 

SICALLBACK XSILoadPlugin( PluginRegistrar& in_reg )
{
	in_reg.PutAuthor(L"Mohammad Abdulfatah");
	in_reg.PutName(L"SO Cubic Bezier Curve Plugin");
	in_reg.PutVersion(1,0);

	RegisterSO_CubicBezierCurve( in_reg );

	//RegistrationInsertionPoint - do not remove this line

	return CStatus::OK;
}

SICALLBACK XSIUnloadPlugin( const PluginRegistrar& in_reg )
{
	CString strPluginName;
	strPluginName = in_reg.GetName();
	return CStatus::OK;
}

CStatus RegisterSO_CubicBezierCurve( PluginRegistrar& in_reg )
{
	ICENodeDef nodeDef;
	nodeDef = Application().GetFactory().CreateICENodeDef(L"SO_CubicBezierCurve",L"SO Cubic Bezier Curve");

	CStatus st;
	st = nodeDef.PutColor(154,188,102);
	st.AssertSucceeded( ) ;


	// Add input ports and groups.
	st = nodeDef.AddPortGroup(ID_G_100);
	st.AssertSucceeded( ) ;

	st = nodeDef.AddInputPort(ID_IN_S,ID_G_100,siICENodeDataFloat,siICENodeStructureSingle,siICENodeContextAny,L"S",L"S",0,CValue(),CValue(),ID_UNDEF,ID_UNDEF,ID_CTXT_CNS);
	st.AssertSucceeded( ) ;

	st = nodeDef.AddInputPort(ID_IN_P0,ID_G_100,siICENodeDataVector3,siICENodeStructureSingle,siICENodeContextAny,L"P0",L"P0",MATH::CVector3f(1.0,1.0,1.0),CValue(),CValue(),ID_UNDEF,ID_UNDEF,ID_CTXT_CNS);
	st.AssertSucceeded( ) ;

	st = nodeDef.AddInputPort(ID_IN_P1,ID_G_100,siICENodeDataVector3,siICENodeStructureSingle,siICENodeContextAny,L"P1",L"P1",MATH::CVector3f(1.0,1.0,1.0),CValue(),CValue(),ID_UNDEF,ID_UNDEF,ID_CTXT_CNS);
	st.AssertSucceeded( ) ;

	st = nodeDef.AddInputPort(ID_IN_P2,ID_G_100,siICENodeDataVector3,siICENodeStructureSingle,siICENodeContextAny,L"P2",L"P2",MATH::CVector3f(1.0,1.0,1.0),CValue(),CValue(),ID_UNDEF,ID_UNDEF,ID_CTXT_CNS);
	st.AssertSucceeded( ) ;

	st = nodeDef.AddInputPort(ID_IN_P3,ID_G_100,siICENodeDataVector3,siICENodeStructureSingle,siICENodeContextAny,L"P3",L"P3",MATH::CVector3f(1.0,1.0,1.0),CValue(),CValue(),ID_UNDEF,ID_UNDEF,ID_CTXT_CNS);
	st.AssertSucceeded( ) ;

	st = nodeDef.AddInputPort(ID_IN_SegmentCount,ID_G_100,siICENodeDataLong,siICENodeStructureSingle,siICENodeContextAny,L"Number of Linear Segments",L"SegmentCount",5,CValue(),CValue(),ID_UNDEF,ID_UNDEF,ID_CTXT_CNS);
	st.AssertSucceeded( ) ;

	// Add output ports.
	st = nodeDef.AddOutputPort(ID_OUT_Position,siICENodeDataVector3,siICENodeStructureSingle,siICENodeContextAny,L"Position",L"Position",ID_UNDEF,ID_UNDEF,ID_CTXT_CNS);
	st.AssertSucceeded( ) ;

	st = nodeDef.AddOutputPort(ID_OUT_Tangent,siICENodeDataVector3,siICENodeStructureSingle,siICENodeContextAny,L"Tangent",L"Tangent",ID_UNDEF,ID_UNDEF,ID_CTXT_CNS);
	st.AssertSucceeded( ) ;

	st = nodeDef.AddOutputPort(ID_OUT_Length,siICENodeDataFloat,siICENodeStructureSingle,siICENodeContextAny,L"Length",L"Length",ID_UNDEF,ID_UNDEF,ID_CTXT_CNS);
	st.AssertSucceeded( ) ;

	PluginItem nodeItem = in_reg.RegisterICENode(nodeDef);
	nodeItem.PutCategories(L"SO Kinematic Solvers");

	return CStatus::OK;
}

SICALLBACK SO_CubicBezierCurve_Evaluate( ICENodeContext& in_ctxt )
{
	// The current output port being evaluated...
	ULONG out_portID = in_ctxt.GetEvaluatedOutputPortID( );
  
	switch( out_portID )
	{		
		case ID_OUT_Position :
		{
			// Get the output port array ...			
			CDataArrayVector3f outData( in_ctxt );
			
 			// Get the input data buffers for each port
			CDataArrayFloat SData( in_ctxt, ID_IN_S );
			CDataArrayVector3f P0Data( in_ctxt, ID_IN_P0 );
			CDataArrayVector3f P1Data( in_ctxt, ID_IN_P1 );
			CDataArrayVector3f P2Data( in_ctxt, ID_IN_P2 );
			CDataArrayVector3f P3Data( in_ctxt, ID_IN_P3 );
			CDataArrayLong SegmentCountData( in_ctxt, ID_IN_SegmentCount);

 			// We need a CIndexSet to iterate over the data 		
			CIndexSet indexSet( in_ctxt );
			for(CIndexSet::Iterator it = indexSet.Begin(); it.HasNext(); it.Next())
			{
				// Add code to set output port...
				ULONG index = it;
				
				float s = SData[index];
				XSI::MATH::CVector3f p0 = P0Data[index];
				XSI::MATH::CVector3f p1 = P1Data[index];
				XSI::MATH::CVector3f p2 = P2Data[index];
				XSI::MATH::CVector3f p3 = P3Data[index];
				long segmentCount = SegmentCountData[index];
				if (segmentCount < 1)
					segmentCount = 1;

				std::vector<float> lengths;
				ComputeLinearSegmentsLengths(segmentCount, p0, p1, p2, p3, &lengths);
				float t = 0.0f;

				for (int i = 1; i <= segmentCount; ++i)
				{
					float tnMinusOne = 1.0f * (i-1) / segmentCount;
					float tn = 1.0f * i / segmentCount;

					if (s >= (lengths[i-1]/lengths[segmentCount]) && s <= (lengths[i]/lengths[segmentCount]))
					{
						t = tnMinusOne + (s - lengths[i-1]/lengths[segmentCount]) * (tn - tnMinusOne) / (lengths[i]/lengths[segmentCount] - lengths[i-1]/lengths[segmentCount]);
					}
				}

				XSI::MATH::CVector3f p;

				ComputePositionOnBezeirCurve(t, p0, p1, p2, p3, &p);												
				outData[index].Set(p.GetX(), p.GetY(), p.GetZ());
			}
		}
		break;

		case ID_OUT_Tangent:
		{
			// Get the output port array ...			
			CDataArrayVector3f outData( in_ctxt );
			
 			// Get the input data buffers for each port
			CDataArrayFloat SData( in_ctxt, ID_IN_S );
			CDataArrayVector3f P0Data( in_ctxt, ID_IN_P0 );
			CDataArrayVector3f P1Data( in_ctxt, ID_IN_P1 );
			CDataArrayVector3f P2Data( in_ctxt, ID_IN_P2 );
			CDataArrayVector3f P3Data( in_ctxt, ID_IN_P3 );
			CDataArrayLong SegmentCountData( in_ctxt, ID_IN_SegmentCount);

 			// We need a CIndexSet to iterate over the data 		
			CIndexSet indexSet( in_ctxt );
			for(CIndexSet::Iterator it = indexSet.Begin(); it.HasNext(); it.Next())
			{
				// Add code to set output port...
				ULONG index = it;
				
				float s = SData[index];
				if (s < 0.0f)
					s = 0.0f;
				if (s > 1.0f)
					s = 1.0f;

				XSI::MATH::CVector3f p0 = P0Data[index];
				XSI::MATH::CVector3f p1 = P1Data[index];
				XSI::MATH::CVector3f p2 = P2Data[index];
				XSI::MATH::CVector3f p3 = P3Data[index];
				long segmentCount = SegmentCountData[index];
				if (segmentCount < 1)
					segmentCount = 1;

				std::vector<float> lengths;
				ComputeLinearSegmentsLengths(segmentCount, p0, p1, p2, p3, &lengths);
				float t = 0.0f;

				for (int i = 1; i <= segmentCount; ++i)
				{
					float tnMinusOne = 1.0f * (i-1) / segmentCount;
					float tn = 1.0f * i / segmentCount;

					if (s >= (lengths[i-1]/lengths[segmentCount]) && s <= (lengths[i]/lengths[segmentCount]))
					{
						t = tnMinusOne + (s - lengths[i-1]/lengths[segmentCount]) * (tn - tnMinusOne) / (lengths[i]/lengths[segmentCount] - lengths[i-1]/lengths[segmentCount]);
					}
				}

				XSI::MATH::CVector3f tangent;
				ComputeTangentOnBezeirCurve(t, p0, p1, p2, p3, &tangent);

				outData[index].Set(tangent.GetX(), tangent.GetY(), tangent.GetZ());
			}
		}
		break;
		
		case ID_OUT_Length:
		{
			// Get the output port array ...			
			CDataArrayFloat outData( in_ctxt );
			
 			// Get the input data buffers for each port
			CDataArrayFloat SData( in_ctxt, ID_IN_S );
			CDataArrayVector3f P0Data( in_ctxt, ID_IN_P0 );
			CDataArrayVector3f P1Data( in_ctxt, ID_IN_P1 );
			CDataArrayVector3f P2Data( in_ctxt, ID_IN_P2 );
			CDataArrayVector3f P3Data( in_ctxt, ID_IN_P3 );
			CDataArrayLong SegmentCountData( in_ctxt, ID_IN_SegmentCount);

 			// We need a CIndexSet to iterate over the data 		
			CIndexSet indexSet( in_ctxt );
			for(CIndexSet::Iterator it = indexSet.Begin(); it.HasNext(); it.Next())
			{
				ULONG index = it;
				
				float s = SData[index];
				if (s < 0.0f)
					s = 0.0f;
				if (s > 1.0f)
					s = 1.0f;

				XSI::MATH::CVector3f p0 = P0Data[index];
				XSI::MATH::CVector3f p1 = P1Data[index];
				XSI::MATH::CVector3f p2 = P2Data[index];
				XSI::MATH::CVector3f p3 = P3Data[index];
				long segmentCount = SegmentCountData[index];
				if (segmentCount < 1)
					segmentCount = 1;

				std::vector<float> lengths;
				ComputeLinearSegmentsLengths(segmentCount, p0, p1, p2, p3, &lengths);
				
				float lengthAtS = s * lengths[segmentCount];

				outData[index] = lengthAtS;
			}
		}
		
		// Other output ports...
	};
	
	return CStatus::OK;
}

void ComputeLinearSegmentsLengths
	( int segmentCount
	, const XSI::MATH::CVector3f& p0
	, const XSI::MATH::CVector3f& p1
	, const XSI::MATH::CVector3f& p2
	, const XSI::MATH::CVector3f& p3
	, std::vector<float>* lengths
	)
{
	lengths->clear();
	lengths->push_back(0.0f);
	
	XSI::MATH::CVector3f previousPointPosition(0.0f, 0.0f, 0.0f);
	
	for (int i = 1; i <= segmentCount; ++i)
	{
		float s = 1.0f * i / segmentCount;
		XSI::MATH::CVector3f currentPointPosition;
		ComputePositionOnBezeirCurve(s, p0, p1, p2, p3, &currentPointPosition);
		XSI::MATH::CVector3f currentSegtmentVector(currentPointPosition);
		currentSegtmentVector.SubInPlace(previousPointPosition);
		float currentSegmentLength = currentSegtmentVector.GetLength();
		lengths->push_back((*lengths)[i-1] + currentSegmentLength);
		previousPointPosition = currentPointPosition;
	}
}

void ComputePositionOnBezeirCurve
	( float s
	, const XSI::MATH::CVector3f& p0
	, const XSI::MATH::CVector3f& p1
	, const XSI::MATH::CVector3f& p2
	, const XSI::MATH::CVector3f& p3
	, XSI::MATH::CVector3f* p
	)
{
	float sSquare = s * s;
	float sCube = sSquare * s;

	XSI::MATH::CVector3f p0Term;
	p0Term.Scale(1.0f - 3.0f*s + 3.0f*sSquare - sCube, p0);

	XSI::MATH::CVector3f p1Term;
	p1Term.Scale(3.0f*s - 6.0f*sSquare + 3.0f*sCube, p1);

	XSI::MATH::CVector3f p2Term;
	p2Term.Scale(3.0f*sSquare - 3.0f*sCube, p2);

	XSI::MATH::CVector3f p3Term;
	p3Term.Scale(sCube, p3);
	
	p->Add(p0Term, p1Term);
	p->AddInPlace(p2Term);
	p->AddInPlace(p3Term);	
}

void ComputeTangentOnBezeirCurve
	( float s
	, const XSI::MATH::CVector3f& p0
	, const XSI::MATH::CVector3f& p1
	, const XSI::MATH::CVector3f& p2
	, const XSI::MATH::CVector3f& p3
	, XSI::MATH::CVector3f* t
	)
{
	float sSquare = s * s;

	XSI::MATH::CVector3f p0Term;
	p0Term.Scale(-3.0f + 6.0f*s - 3.0f*sSquare, p0);

	XSI::MATH::CVector3f p1Term;
	p1Term.Scale(3.0f - 12.0f*s + 9.0f*sSquare, p1);

	XSI::MATH::CVector3f p2Term;
	p2Term.Scale(6.0f*s - 9.0f*sSquare, p2);

	XSI::MATH::CVector3f p3Term;
	p3Term.Scale(3.0f * sSquare, p3);

	t->Add(p0Term, p1Term);
	t->AddInPlace(p2Term);
	t->AddInPlace(p3Term);
}
